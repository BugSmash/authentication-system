<?php
return [
    /*** NavBar ***/ 
    'home' => 'صفحه اصلی',
    'login' => 'ورود',
    'register' => 'ثبت نام',
    'profile' => 'پروفایل',
    'search' => 'جستجو',
    'logout' => 'خروج',
    /*** Login Form ***/
    'login form' => 'فرم ورود',
    'email address' => 'آدرس ایمیل',
    'enter email' => 'ایمیل',
    'email privacy' => 'اطلاعات شما محرمانه نزد ما حفظ خواهد شد',
    'password' => 'رمز عبور',
    'check me out' => 'مرا به خاطر بسپار',
    'submit' => 'ورود',
    /*** Register Form ***/
    'register form' => 'فرم ثبت نام',
    'user name' => 'نام و نام خانوادگی',
    'confirm password' => 'تایید رمز عبور',
    'tel' => 'شماره تلفن',
    'register' => 'ثبت نام',
];
?>