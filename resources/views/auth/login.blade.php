@extends('layouts.app')

@section('title','ورود کاربر')

@section('content')
    <div class="row justify-content-center" style="padding-top: 40px;">
        <div class="col-md-6" style="background:aliceblue; padding: 15px 20px; border-radius: 7px;">
            <form action="{{ route('auth.login.form', ['id'=>1]) }}" method="post">
                <h3>@lang('theme.login form')</h3>
                <hr />
                <div class="form-group">
                  <label for="exampleInputEmail1">@lang('theme.email address')</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="@lang('theme.enter email')">
                  <small id="emailHelp" class="form-text text-muted">@lang('theme.email privacy')</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">@lang('theme.password')</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="@lang('theme.password')">
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1" style="margin-right: 20px;">@lang('theme.check me out')</label>
                </div>
                <button type="submit" class="btn btn-primary" style="margin-top: 15px;">@lang('theme.submit')</button>
              </form>
        </div>
    </div>
@endsection