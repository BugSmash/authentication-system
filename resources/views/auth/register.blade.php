@extends('layouts.app')
@section('title','ثبت نام کاربر')
@section('content')
    <div class="row justify-content-center" style="padding-top: 40px;">
        <div class="col-md-6" style="background:aliceblue; padding: 15px 20px; border-radius: 7px;">
            <form action="{{ route('auth.login.form', ['id'=>1]) }}" method="post">
                <h3>@lang('theme.register form')</h3>
                <hr />
                <div class="form-group">
                  <label for="exampleInputEmail1">@lang('theme.email address')</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="@lang('theme.enter email')">
                  <small id="emailHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="name">@lang('theme.user name')</label>
                    <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="@lang('theme.user name')">
                    <small id="nameHelp" class="form-text text-muted"></small>
                  </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">@lang('theme.password')</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="@lang('theme.password')">
                </div>
                <div class="form-group">
                    <label for="re-password">@lang('theme.password')</label>
                    <input type="password" class="form-control" id="re-password" name="re-password" placeholder="@lang('theme.confirm password')">
                </div>
                <div class="form-group">
                    <label for="tel">@lang('theme.password')</label>
                    <input type="text" class="form-control" id="tel" name="tel" placeholder="@lang('theme.tel')">
                </div>
                <button type="submit" class="btn btn-primary" style="margin-top: 15px;">@lang('theme.register')</button>
              </form>
        </div>
    </div>
@endsection